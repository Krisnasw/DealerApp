package com.luwakdev.dealer.promo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.luwakdev.dealer.R;
import com.luwakdev.dealer.barang.BarangActivity;
import com.luwakdev.dealer.barang.BarangAdapter;
import com.luwakdev.dealer.data.model.ApiPromo;
import com.luwakdev.dealer.data.model.Barang;
import com.luwakdev.dealer.data.model.HomeGridModelClass;
import com.luwakdev.dealer.data.model.Promo;
import com.luwakdev.dealer.data.remote.ApiClient;
import com.luwakdev.dealer.util.AppConfig;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PromoFragment extends Fragment {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    private ArrayList<HomeGridModelClass> homeGridModelClasses;

    private PromoAdapter mAdapter;

    private Integer image[] = {R.drawable.hot_deals,R.drawable.hot_deals,R.drawable.hot_deals,R.drawable.hot_deals,
            R.drawable.hot_deals, R.drawable.hot_deals,R.drawable.hot_deals,R.drawable.hot_deals,};
    private String name[] = {"Promo Menarik","Promo Menarik","Promo Menarik","Promo Menarik","Promo Menarik","Promo Menarik","Promo Menarik","Promo Menarik"};

    public PromoFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_promo, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ButterKnife.bind(this, view);

        getPromoData();

        return view;
    }

    private void getPromoData() {
        ApiClient.get(getActivity()).getPromoData(Prefs.getString(AppConfig.KEY_TOKEN, "")).enqueue(new Callback<ApiPromo>() {
            @Override
            public void onResponse(Call<ApiPromo> call, Response<ApiPromo> response) {
                ApiPromo resp = response.body();

                if (resp.getStatus() == "false") {
                    recyclerView.setVisibility(View.GONE);
                } else {
                    List<Promo> barangs = resp.getPromos();

                    LinearLayoutManager linearVertical = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);

                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(linearVertical);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(new PromoAdapter(getContext(), barangs));
                }
            }

            @Override
            public void onFailure(Call<ApiPromo> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}