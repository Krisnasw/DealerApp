package com.luwakdev.dealer.promo;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.luwakdev.dealer.R;
import com.luwakdev.dealer.data.model.HomeGridModelClass;
import com.luwakdev.dealer.data.model.Promo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PromoAdapter extends RecyclerView.Adapter<PromoAdapter.MyViewHolder> {

    Context context;

    private List<Promo> moviesList;
    Unbinder unbinder;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView imageView;

        @BindView(R.id.tvPromoName)
        TextView title;

        public MyViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);
            unbinder = ButterKnife.bind(this, view);

        }

    }

    public PromoAdapter(Context mainActivityContacts, List<Promo> moviesList) {
        this.moviesList = moviesList;
        this.context = mainActivityContacts;
    }

    @Override
    public PromoAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_promo, parent, false);

        return new PromoAdapter.MyViewHolder(itemView);
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final PromoAdapter.MyViewHolder holder, int position) {
        Promo movie = moviesList.get(position);
        holder.title.setText(movie.getBonus_name());

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_placeholder);
        requestOptions.error(R.drawable.ic_error);

        Glide.with(context)
                .load("https://sahidhotels.co.id/wp-content/uploads/2017/06/Sahid-Rich-Jogja-Berikan-Promo-All-Day-Package2.png")
                .apply(requestOptions)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}