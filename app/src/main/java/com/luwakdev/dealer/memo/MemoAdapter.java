package com.luwakdev.dealer.memo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.luwakdev.dealer.R;
import com.luwakdev.dealer.data.model.ApiMemo;
import com.luwakdev.dealer.data.model.Barang;
import com.luwakdev.dealer.data.model.Memo;
import com.luwakdev.dealer.data.remote.ApiClient;
import com.luwakdev.dealer.util.AppConfig;
import com.pixplicity.easyprefs.library.Prefs;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemoAdapter extends RecyclerView.Adapter<MemoAdapter.MyViewHolder> {

    Context context;
    private List<Memo> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView title, stock, code;

        public MyViewHolder(View view) {
            super(view);

            cv = (CardView) view.findViewById(R.id.cv);
            title = (TextView) view.findViewById(R.id.namaBarang);
            stock = (TextView) view.findViewById(R.id.qtyBarang);
            code = (TextView) view.findViewById(R.id.diskonBarang);

        }

    }

    public MemoAdapter(Context mainActivityContacts, List<Memo> moviesList) {
        this.moviesList = moviesList;
        this.context = mainActivityContacts;
    }

    @Override
    public MemoAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_memo, parent, false);

        return new MemoAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MemoAdapter.MyViewHolder holder, int position) {
        final Memo movie = moviesList.get(position);
        holder.title.setText(movie.getItem_name().trim());
        holder.stock.setText("Quantity : " + String.valueOf(movie.getMemo_detail_qty()));
        holder.code.setText("Akumulasi : " + movie.getMemo_accumulation());

        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editMemoDetail(movie.getMemo_detail_id());
            }
        });

        holder.cv.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Hapus Barang")
                        .setConfirmText("Apakah Anda Yakin Menghapus "+movie.getItem_name().trim())
                        .setConfirmText("Ya")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                deleteMemoDetail(movie.getMemo_detail_id());
                            }
                        })
                        .setCancelText("Tidak")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();

                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private void deleteMemoDetail(Integer id) {
        ApiClient.get(context).deleteMemoDetail(Prefs.getString(AppConfig.KEY_TOKEN, ""), id).enqueue(new Callback<ApiMemo>() {
            @Override
            public void onResponse(Call<ApiMemo> call, Response<ApiMemo> response) {
                ApiMemo resp = response.body();

                if (resp.getStatus() == "false") {
                    new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setConfirmText(resp.getMessage())
                            .show();
                } else {
                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    context.startActivity(new Intent(context, MemoActivity.class));
                                    ((Activity)context).finish();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiMemo> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void editMemoDetail(final Integer ids) {
        final MaterialEditText editText = new MaterialEditText(context);
        editText.setHint("Masukkan Quantity");
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText("Edit Barang")
                .setConfirmText("Submit")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(final SweetAlertDialog sweetAlertDialog) {
                        ApiClient.get(context).updateMemoDetail(Prefs.getString(AppConfig.KEY_TOKEN, ""), ids , Integer.valueOf(editText.getText().toString()))
                                .enqueue(new Callback<ApiMemo>() {
                                    @Override
                                    public void onResponse(Call<ApiMemo> call, Response<ApiMemo> response) {
                                        ApiMemo resp = response.body();

                                        if (resp.getStatus() == "false") {
                                            sweetAlertDialog.dismissWithAnimation();
                                            new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                                                    .setTitleText("Notification")
                                                    .setConfirmText(resp.getMessage())
                                                    .show();
                                        } else {
                                            sweetAlertDialog.dismissWithAnimation();
                                            new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                                                    .setTitleText("Notification")
                                                    .setContentText(resp.getMessage())
                                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                        @Override
                                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                            context.startActivity(new Intent(context, MemoActivity.class));
                                                            ((Activity)context).finish();
                                                        }
                                                    })
                                                    .show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ApiMemo> call, Throwable t) {
                                        try {
                                            throw new InterruptedException("Gagal Connect Server");
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                    }
                })
                .setCancelText("Cancel")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .setCustomView(editText)
                .show();
    }
}