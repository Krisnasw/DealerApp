package com.luwakdev.dealer.memo;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.luwakdev.dealer.R;
import com.luwakdev.dealer.data.model.ApiBarang;
import com.luwakdev.dealer.data.model.ApiMemoDetail;
import com.luwakdev.dealer.data.model.Barang;
import com.luwakdev.dealer.data.remote.ApiClient;
import com.luwakdev.dealer.util.AppConfig;
import com.pixplicity.easyprefs.library.Prefs;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemoDetailActivity extends AppCompatActivity {

    @BindView(R.id.customerDiskon)
    MaterialEditText customerDiskon;
    @BindView(R.id.customerQty)
    MaterialEditText customerQty;
    @BindView(R.id.spItem)
    SearchableSpinner spItem;
    @BindView(R.id.btnAddItem)
    Button btnAddItem;

    private SweetAlertDialog swal;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spItem.setTitle("Pilih Barang");
        spItem.setPositiveButton("Ok");

        initSpinnerItem();

        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swal = new SweetAlertDialog(MemoDetailActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                swal.setTitleText("Loading");
                swal.setContentText("Processing...");
                swal.setCancelable(false);
                swal.show();
                postMemoDetail(Prefs.getString(AppConfig.KEY_TOKEN, ""), String.valueOf(spItem.getSelectedItem()), customerQty.getText().toString(), customerDiskon.getText().toString());
            }
        });
    }

    private void initSpinnerItem() {
        ApiClient.get(this).getListBarang(Prefs.getString(AppConfig.KEY_TOKEN, "")).enqueue(new Callback<ApiBarang>() {
            @Override
            public void onResponse(Call<ApiBarang> call, Response<ApiBarang> response) {
                ApiBarang resp = response.body();

                if (resp.getStatus() == "false") {
                    new SweetAlertDialog(MemoDetailActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                } else {
                    List<Barang> grades = resp.getBarangs();
                    List<String> strings = new ArrayList<String>();

                    for (int i = 0; i < grades.size(); i++) {
                        strings.add(grades.get(i).getItem_code() +" - "+ grades.get(i).getItem_name());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(MemoDetailActivity.this,
                            android.R.layout.simple_spinner_dropdown_item, strings);
                    spItem.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ApiBarang> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void postMemoDetail(String token, String item, String qty, String diskon) {
        ApiClient.get(this).createMemoDetail(token, item, qty, diskon).enqueue(new Callback<ApiMemoDetail>() {
            @Override
            public void onResponse(Call<ApiMemoDetail> call, Response<ApiMemoDetail> response) {
                ApiMemoDetail resp = response.body();

                if (resp.getStatus() == "false") {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(MemoDetailActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                } else {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(MemoDetailActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    startActivity(new Intent(MemoDetailActivity.this, MemoActivity.class));
                                    finish();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiMemoDetail> call, Throwable t) {
                try {
                    swal.dismissWithAnimation();
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
