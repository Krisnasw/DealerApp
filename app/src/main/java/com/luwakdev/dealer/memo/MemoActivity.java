package com.luwakdev.dealer.memo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.luwakdev.dealer.R;
import com.luwakdev.dealer.barang.BarangActivity;
import com.luwakdev.dealer.barang.BarangAdapter;
import com.luwakdev.dealer.customer.CustomerActivity;
import com.luwakdev.dealer.data.model.ApiCity;
import com.luwakdev.dealer.data.model.ApiGrade;
import com.luwakdev.dealer.data.model.ApiListCustomer;
import com.luwakdev.dealer.data.model.ApiListMemoDetail;
import com.luwakdev.dealer.data.model.ApiMemo;
import com.luwakdev.dealer.data.model.ApiMerk;
import com.luwakdev.dealer.data.model.ApiSales;
import com.luwakdev.dealer.data.model.Barang;
import com.luwakdev.dealer.data.model.Customer;
import com.luwakdev.dealer.data.model.Grade;
import com.luwakdev.dealer.data.model.Memo;
import com.luwakdev.dealer.data.model.Merk;
import com.luwakdev.dealer.data.model.Sales;
import com.luwakdev.dealer.data.remote.ApiClient;
import com.luwakdev.dealer.main.MainActivity;
import com.luwakdev.dealer.util.AppConfig;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MemoActivity extends AppCompatActivity {

    @BindView(R.id.spCustomer)
    Spinner spCustomer;
    @BindView(R.id.spMerk)
    Spinner spMerk;
    @BindView(R.id.spSales)
    Spinner spSales;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.imgNotFound)
    ImageView imgNotFound;
    @BindView(R.id.tvNotFound)
    TextView tvNotFound;

    private SweetAlertDialog swal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memo);

        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spCustomer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                initSpinnerCustomer();
                return false;
            }
        });

        spMerk.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                initSpinnerMerk();
                return false;
            }
        });

        spSales.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                initSpinnerSales();
                return false;
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MemoActivity.this, MemoDetailActivity.class));
            }
        });

        progressBar.setVisibility(View.VISIBLE);
        progressBar.setIndeterminate(true);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swal = new SweetAlertDialog(MemoActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                swal.setTitleText("Loading");
                swal.setContentText("Processing...");
                swal.show();
                createMemo();
            }
        });

        initListMemoDetail();
    }

    private void initSpinnerCustomer() {
        ApiClient.get(this).getListCustomer(Prefs.getString(AppConfig.KEY_TOKEN, "")).enqueue(new Callback<ApiListCustomer>() {
            @Override
            public void onResponse(Call<ApiListCustomer> call, Response<ApiListCustomer> response) {
                ApiListCustomer resp = response.body();

                if (resp.getStatus() == "true") {
                    List<Customer> grades = resp.getCustomers();
                    List<String> strings = new ArrayList<String>();

                    for (int i = 0; i < grades.size(); i++) {
                        strings.add(grades.get(i).getCustomer_name());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(MemoActivity.this,
                            android.R.layout.simple_spinner_item, strings);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spCustomer.setAdapter(adapter);
                } else {
                    new SweetAlertDialog(MemoActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiListCustomer> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initSpinnerMerk() {
        ApiClient.get(this).getListMerk(Prefs.getString(AppConfig.KEY_TOKEN, "")).enqueue(new Callback<ApiMerk>() {
            @Override
            public void onResponse(Call<ApiMerk> call, Response<ApiMerk> response) {
                ApiMerk resp = response.body();

                if (resp.getStatus() == "true") {
                    List<Merk> merks = resp.getMerkList();
                    List<String> strings = new ArrayList<String>();

                    for (int i = 0; i < merks.size(); i++) {
                        strings.add(merks.get(i).getBrand_name());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(MemoActivity.this,
                            android.R.layout.simple_spinner_item, strings);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spMerk.setAdapter(adapter);
                } else {
                    new SweetAlertDialog(MemoActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiMerk> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initSpinnerSales() {
        ApiClient.get(this).getListSales(Prefs.getString(AppConfig.KEY_TOKEN, "")).enqueue(new Callback<ApiSales>() {
            @Override
            public void onResponse(Call<ApiSales> call, Response<ApiSales> response) {
                ApiSales resp = response.body();

                if (resp.getStatus() == "true") {
                    List<Sales> merks = resp.getSales();
                    List<String> strings = new ArrayList<String>();

                    for (int i = 0; i < merks.size(); i++) {
                        strings.add(merks.get(i).getSales_name());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(MemoActivity.this,
                            android.R.layout.simple_spinner_item, strings);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spSales.setAdapter(adapter);
                } else {
                    new SweetAlertDialog(MemoActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiSales> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void initListMemoDetail() {
        ApiClient.get(this).getListMemoDetail(Prefs.getString(AppConfig.KEY_TOKEN, "")).enqueue(new Callback<ApiListMemoDetail>() {
            @Override
            public void onResponse(Call<ApiListMemoDetail> call, Response<ApiListMemoDetail> response) {
                ApiListMemoDetail resp = response.body();

                if (resp.getStatus() == "false") {
                    progressBar.setIndeterminate(false);
                    progressBar.setVisibility(View.GONE);
                    imgNotFound.setVisibility(View.VISIBLE);
                    tvNotFound.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                } else {
                    progressBar.setIndeterminate(false);
                    progressBar.setVisibility(View.GONE);

                    List<Memo> barangs = resp.getMemoList();

                    if (barangs.size() == 0) {
                        imgNotFound.setVisibility(View.VISIBLE);
                        tvNotFound.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }

                    LinearLayoutManager linearVertical = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);

                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(linearVertical);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(new MemoAdapter(MemoActivity.this, barangs));
                }
            }

            @Override
            public void onFailure(Call<ApiListMemoDetail> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void createMemo() {
        ApiClient.get(this).createMemo(Prefs.getString(AppConfig.KEY_TOKEN, ""), spCustomer.getSelectedItem().toString(), spMerk.getSelectedItem().toString(), spSales.getSelectedItem().toString()).enqueue(new Callback<ApiMemo>() {
            @Override
            public void onResponse(Call<ApiMemo> call, Response<ApiMemo> response) {
                ApiMemo resp = response.body();

                if (resp.getStatus() == "false") {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(MemoActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                } else {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(MemoActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    startActivity(new Intent(MemoActivity.this, MainActivity.class));
                                    finish();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiMemo> call, Throwable t) {
                try {
                    swal.dismissWithAnimation();
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}