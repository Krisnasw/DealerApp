package com.luwakdev.dealer.account;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.luwakdev.dealer.R;
import com.luwakdev.dealer.data.model.ApiUser;
import com.luwakdev.dealer.data.remote.ApiClient;
import com.luwakdev.dealer.katalog.KatalogDetailActivity;
import com.luwakdev.dealer.login.LoginActivity;
import com.luwakdev.dealer.util.AppConfig;
import com.pixplicity.easyprefs.library.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountFragment extends Fragment {

    @BindView(R.id.textImage)
    TextView textView;
    @BindView(R.id.fullname)
    TextView fullname;
    @BindView(R.id.yourEmail)
    TextView email;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.btnEdit)
    TextView btnEdit;
    @BindView(R.id.logout)
    LinearLayout btnLogout;

    Unbinder unbinder;

    String userName, userEmail, userPhone;

    public AccountFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container,false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ButterKnife.bind(this, view);
        unbinder = ButterKnife.bind(this, view);

        getUserData();

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Prefs.clear();
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finish();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AccountEditActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("nama", userName);
                bundle.putString("email", userEmail);
                bundle.putString("phone", userPhone);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        return view;
    }

    private void getUserData() {
        ApiClient.get(getActivity()).getUserData(Prefs.getString(AppConfig.KEY_TOKEN, "")).enqueue(new Callback<ApiUser>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ApiUser> call, Response<ApiUser> response) {
                ApiUser resp = response.body();
                if (resp.getStatus().equals("true")) {
                    fullname.setText(""+resp.getData());
                    String awal = resp.getData().substring(0,1);
                    textView.setText(awal);
                    email.setText(resp.getEmail());
                    phone.setText(resp.getPhone());

                    userEmail = resp.getEmail();
                    userName = resp.getData();
                    userPhone = resp.getPhone();
                } else {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText("User Tidak Ditemukan")
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiUser> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
