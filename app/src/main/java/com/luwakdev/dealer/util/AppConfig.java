package com.luwakdev.dealer.util;

public class AppConfig {

//    public static final String API_URL = "http://192.168.123.17/oto/public/api/v1/";
    public static final String API_URL = "http://dealer.intivestudio.com/api/v1/";
    public static final String GPS_STATE = "gps_state";

    public static final String KEY_TOKEN = "token";
    public static final String KEY_ISLOGGEDIN = "is_logged_in";

}