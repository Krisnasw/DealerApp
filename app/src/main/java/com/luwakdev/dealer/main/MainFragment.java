package com.luwakdev.dealer.main;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.luwakdev.dealer.R;
import com.luwakdev.dealer.barang.BarangActivity;
import com.luwakdev.dealer.customer.CustomerActivity;
import com.luwakdev.dealer.data.model.ApiUser;
import com.luwakdev.dealer.data.remote.ApiClient;
import com.luwakdev.dealer.delivery.DeliveryActivity;
import com.luwakdev.dealer.katalog.KatalogActivity;
import com.luwakdev.dealer.memo.MemoActivity;
import com.luwakdev.dealer.scan.ScanActivity;
import com.luwakdev.dealer.util.AppConfig;
import com.pixplicity.easyprefs.library.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainFragment extends Fragment {

    private View view;

    @BindView(R.id.btnCustomer)
    CardView btnCustomer;
    @BindView(R.id.btnBarang)
    CardView btnBarang;
    @BindView(R.id.btnMemo)
    CardView btnMemo;
    @BindView(R.id.btnScan)
    CardView btnScan;
    @BindView(R.id.btnDelivery)
    CardView btnDelivery;
    @BindView(R.id.btnKatalog)
    CardView btnKatalog;

    SweetAlertDialog swal;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ButterKnife.bind(this, view);

        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ScanActivity.class));
            }
        });

        btnBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), BarangActivity.class));
            }
        });

        btnCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), CustomerActivity.class));
            }
        });

        btnMemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MemoActivity.class));
            }
        });

        btnDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), DeliveryActivity.class));
            }
        });

        btnKatalog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), KatalogActivity.class));
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

}