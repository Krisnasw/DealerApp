package com.luwakdev.dealer.login;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.luwakdev.dealer.customer.CustomerActivity;
import com.luwakdev.dealer.data.model.ApiLogin;
import com.luwakdev.dealer.data.remote.ApiClient;
import com.luwakdev.dealer.main.MainActivity;
import com.luwakdev.dealer.R;
import com.luwakdev.dealer.util.AppConfig;
import com.luwakdev.dealer.util.NetworkUtil;
import com.pixplicity.easyprefs.library.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.etUsername)
    EditText etUsername;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.logo)
    ImageView imageView;

    Unbinder unbinder;

    private SweetAlertDialog swal;
    private static final int READ_REQUEST_CODE = 300;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, READ_REQUEST_CODE);

        ButterKnife.bind(this);
        unbinder = ButterKnife.bind(this);

        Glide.with(this).load(R.drawable.logo).into(imageView);

        if (Prefs.getBoolean(AppConfig.KEY_ISLOGGEDIN, false)) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtil.getInstance(getApplicationContext()).isOnline()) {
                    if (etUsername.getText().toString().equals("") && etPassword.getText().toString().equals("")) {
                        etUsername.setError("Username Wajib Diisi");
                        etPassword.setError("Password Masih Kosong");
                    } else {
                        swal = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                        swal.setTitleText("Loading");
                        swal.setContentText("Please wait...");
                        swal.setCancelable(false);
                        swal.show();

                        ApiClient.get(LoginActivity.this).login(etUsername.getText().toString(), etPassword.getText().toString()).enqueue(new Callback<ApiLogin>() {
                            @Override
                            public void onResponse(Call<ApiLogin> call, Response<ApiLogin> response) {
                                final ApiLogin resp = response.body();
                                if (resp.getStatus().equals("false")) {
                                    swal.dismissWithAnimation();
                                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText("Notification")
                                            .setContentText(resp.getMessage())
                                            .show();
                                } else {
                                    swal.dismissWithAnimation();
                                    new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                            .setTitleText("Notification")
                                            .setContentText("Login Berhasil")
                                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                                @Override
                                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                    insertLoginData(resp.getMessage());
                                                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                                    finish();
                                                }
                                            })
                                            .show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ApiLogin> call, Throwable t) {
                                try {
                                    throw new InterruptedException("Gagal Connect Server");
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Tidak Ada Koneksi Internet", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void insertLoginData(String token) {
        Prefs.putString(AppConfig.KEY_TOKEN, token);
        Prefs.putBoolean(AppConfig.KEY_ISLOGGEDIN, true);
    }
}