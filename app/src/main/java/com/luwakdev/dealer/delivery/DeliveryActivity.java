package com.luwakdev.dealer.delivery;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.luwakdev.dealer.R;
import com.luwakdev.dealer.custom.SublimePickerFragment;
import com.luwakdev.dealer.data.model.ApiSales;
import com.luwakdev.dealer.data.model.ApiSalesCost;
import com.luwakdev.dealer.data.model.ApiSalesCostView;
import com.luwakdev.dealer.data.model.Sales;
import com.luwakdev.dealer.data.model.SalesCost;
import com.luwakdev.dealer.data.model.SalesCostDetail;
import com.luwakdev.dealer.data.remote.ApiClient;
import com.luwakdev.dealer.main.MainActivity;
import com.luwakdev.dealer.util.AppConfig;
import com.pixplicity.easyprefs.library.Prefs;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeliveryActivity extends AppCompatActivity implements SublimePickerFragment.OnDateRangeSelectedListener {

    @BindView(R.id.startDate)
    MaterialEditText startDate;
    @BindView(R.id.spSales)
    Spinner spinner;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.keterangan)
    MaterialEditText keterangan;
    @BindView(R.id.modalAwal)
    MaterialEditText modal;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    Unbinder unbinder;

    private SweetAlertDialog swal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);

        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        unbinder = ButterKnife.bind(this);

        initSpinnerSales();

        startDate.requestFocus();
        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SublimePickerFragment dateRangePickerFragment= SublimePickerFragment.newInstance(DeliveryActivity.this,false);
                dateRangePickerFragment.show(getSupportFragmentManager(),"datePicker");
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                initSalesData(selectedItem);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swal = new SweetAlertDialog(DeliveryActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                swal.setTitleText("Loading");
                swal.setContentText("Processing...");
                swal.setCancelable(false);
                swal.show();
                postBiayaSales();
            }
        });
    }

    @Override
    public void onDateRangeSelected(int startDay, int startMonth, int startYear, int endDay, int endMonth, int endYear) {
        startDate.setText(startDay+"-"+startMonth+"-"+startYear+" - "+endDay+"-"+endMonth+"-"+endYear );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    private void initSalesData(String selecteditem) {
        ApiClient.get(this).getSalesCostDetailBySales(Prefs.getString(AppConfig.KEY_TOKEN, ""), selecteditem).enqueue(new Callback<ApiSalesCostView>() {
            @Override
            public void onResponse(Call<ApiSalesCostView> call, Response<ApiSalesCostView> response) {
                ApiSalesCostView resp = response.body();

                if (resp.getStatus() == "false") {
                    Toast.makeText(DeliveryActivity.this, resp.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    List<SalesCost> barangs = resp.getSalesCosts();

                    if (barangs.size() == 0) {
                        recyclerView.setVisibility(View.GONE);
                    }

                    LinearLayoutManager linearVertical = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);

                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(linearVertical);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(new DeliveryAdapter(DeliveryActivity.this, barangs));
                }
            }

            @Override
            public void onFailure(Call<ApiSalesCostView> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initSpinnerSales() {
        ApiClient.get(this).getListSales(Prefs.getString(AppConfig.KEY_TOKEN, "")).enqueue(new Callback<ApiSales>() {
            @Override
            public void onResponse(Call<ApiSales> call, Response<ApiSales> response) {
                ApiSales resp = response.body();

                if (resp.getStatus() == "true") {
                    List<Sales> merks = resp.getSales();
                    List<String> strings = new ArrayList<String>();

                    for (int i = 0; i < merks.size(); i++) {
                        strings.add(merks.get(i).getSales_name());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(DeliveryActivity.this,
                            android.R.layout.simple_spinner_dropdown_item, strings);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(adapter);
                } else {
                    new SweetAlertDialog(DeliveryActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiSales> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void postBiayaSales() {
        ApiClient.get(this).postBiaya(Prefs.getString(AppConfig.KEY_TOKEN, ""), startDate.getText().toString(), modal.getText().toString(), String.valueOf(spinner.getSelectedItem()),keterangan.getText().toString()).enqueue(new Callback<ApiSalesCost>() {
            @Override
            public void onResponse(Call<ApiSalesCost> call, Response<ApiSalesCost> response) {
                final ApiSalesCost resp = response.body();

                if (resp.getStatus() == "false") {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(DeliveryActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                } else {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(DeliveryActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    Intent i = new Intent(DeliveryActivity.this, DeliveryDetailActivity.class);
                                    i.putExtra("code", resp.getCode());
                                    startActivity(i);
                                    finish();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiSalesCost> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
