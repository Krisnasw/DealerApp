package com.luwakdev.dealer.delivery;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.luwakdev.dealer.R;
import com.luwakdev.dealer.data.model.ApiCost;
import com.luwakdev.dealer.data.model.ApiSalesCost;
import com.luwakdev.dealer.data.model.ApiSalesCostGet;
import com.luwakdev.dealer.data.model.Cost;
import com.luwakdev.dealer.data.model.SalesCostDetail;
import com.luwakdev.dealer.data.remote.ApiClient;
import com.luwakdev.dealer.main.MainActivity;
import com.luwakdev.dealer.util.AppConfig;
import com.pixplicity.easyprefs.library.Prefs;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeliveryDetailActivity2 extends AppCompatActivity {

    @BindView(R.id.tvSales)
    TextView tvSales;
    @BindView(R.id.tvModal)
    TextView tvModal;
    @BindView(R.id.tvPeriode)
    TextView tvPeriode;
    @BindView(R.id.tvKeterangan)
    TextView tvKeterangan;
    @BindView(R.id.spKeperluan)
    SearchableSpinner spinner;
    @BindView(R.id.tanggal)
    MaterialEditText metTanggal;
    @BindView(R.id.totalBiaya)
    MaterialEditText totalBiaya;
    @BindView(R.id.keterangan)
    MaterialEditText keterangan;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.btnLock)
    Button btnLock;

    Unbinder unbinder;

    private DatePickerDialog fromStartDate;
    private Integer code;
    private SweetAlertDialog swal, swal2;
    private String salesNama, keterangans, modalawal, periodes;
    private String less = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_biaya);

        ButterKnife.bind(this);

        unbinder = ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        swal = new SweetAlertDialog(DeliveryDetailActivity2.this, SweetAlertDialog.PROGRESS_TYPE);
        swal.setTitleText("Loading");
        swal.setContentText("Memuat Data...");
        swal.setCancelable(false);
        swal.show();

        Calendar newCalendar = Calendar.getInstance();

        fromStartDate = new DatePickerDialog(DeliveryDetailActivity2.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

                metTanggal.setText(" "+year+"-"+(monthOfYear + 1 )+"-"+dayOfMonth);
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        metTanggal.setInputType(InputType.TYPE_NULL);
        metTanggal.requestFocus();
        metTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromStartDate.show();
            }
        });

        Bundle extras = getIntent().getExtras();
        code = extras.getInt("code");
        salesNama = extras.getString("sales");
        keterangans = extras.getString("keterangan");
        modalawal = extras.getString("modal");
        periodes = extras.getString("periode");

        tvSales.setText("Sales : "+salesNama);
        tvPeriode.setText("Periode : "+periodes);
        tvModal.setText("Modal : "+modalawal);
        tvKeterangan.setText("Keterangan : "+keterangans);

        initKeperluan();
        initSalesCostData();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swal2 = new SweetAlertDialog(DeliveryDetailActivity2.this, SweetAlertDialog.PROGRESS_TYPE);
                swal2.setTitleText("Loading");
                swal2.setContentText("Memuat Data...");
                swal2.setCancelable(false);
                swal2.show();
                postData();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MaterialEditText editText = new MaterialEditText(DeliveryDetailActivity2.this);
                editText.setHint("Masukkan Modal Tambahan");
                editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                new SweetAlertDialog(DeliveryDetailActivity2.this, SweetAlertDialog.NORMAL_TYPE)
                        .setTitleText("Modal Tambahan")
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                less = editText.getText().toString();
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .setCancelText("Batal")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .setCustomView(editText)
                        .show();
            }
        });

        btnLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swal2 = new SweetAlertDialog(DeliveryDetailActivity2.this, SweetAlertDialog.PROGRESS_TYPE);
                swal2.setTitleText("Loading");
                swal2.setContentText("Memuat Data...");
                swal2.setCancelable(false);
                swal2.show();
                lockData();
            }
        });
    }

    private void lockData() {
        ApiClient.get(this).lockSalesCost(Prefs.getString(AppConfig.KEY_TOKEN, ""), code, less).enqueue(new Callback<ApiSalesCost>() {
            @Override
            public void onResponse(Call<ApiSalesCost> call, Response<ApiSalesCost> response) {
                ApiSalesCost resp = response.body();

                if (resp.getStatus() == "false") {
                    swal2.dismissWithAnimation();
                    new SweetAlertDialog(DeliveryDetailActivity2.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                } else {
                    swal2.dismissWithAnimation();
                    new SweetAlertDialog(DeliveryDetailActivity2.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    startActivity(new Intent(DeliveryDetailActivity2.this, MainActivity.class));
                                    finish();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiSalesCost> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void postData() {
        ApiClient.get(this).postCostDetail(Prefs.getString(AppConfig.KEY_TOKEN, ""), code, salesNama, String.valueOf(spinner.getSelectedItem()), metTanggal.getText().toString(), totalBiaya.getText().toString(), keterangan.getText().toString()).enqueue(new Callback<ApiSalesCost>() {
            @Override
            public void onResponse(Call<ApiSalesCost> call, Response<ApiSalesCost> response) {
                ApiSalesCost resp = response.body();

                if (resp.getStatus() == "false") {
                    swal2.dismissWithAnimation();
                    new SweetAlertDialog(DeliveryDetailActivity2.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                } else {
                    swal2.dismissWithAnimation();
                    new SweetAlertDialog(DeliveryDetailActivity2.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    startActivity(new Intent(DeliveryDetailActivity2.this, MainActivity.class));
                                    finish();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiSalesCost> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initSalesCostData() {
        ApiClient.get(this).getSalesCostData(Prefs.getString(AppConfig.KEY_TOKEN, ""), Integer.valueOf(code)).enqueue(new Callback<ApiSalesCostGet>() {
            @Override
            public void onResponse(Call<ApiSalesCostGet> call, Response<ApiSalesCostGet> response) {
                ApiSalesCostGet resp = response.body();

                if (resp.getStatus() == "false") {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(DeliveryDetailActivity2.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                } else {
                    swal.dismissWithAnimation();

                    List<SalesCostDetail> barangs = resp.getSalesCostDetailList();

                    if (barangs.size() == 0) {
                        recyclerView.setVisibility(View.GONE);
                    }

                    LinearLayoutManager linearVertical = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);

                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(linearVertical);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(new DeliveryDetailAdapter(DeliveryDetailActivity2.this, barangs));
                }
            }

            @Override
            public void onFailure(Call<ApiSalesCostGet> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initKeperluan() {
        ApiClient.get(this).getKeperluan(Prefs.getString(AppConfig.KEY_TOKEN, "")).enqueue(new Callback<ApiCost>() {
            @Override
            public void onResponse(Call<ApiCost> call, Response<ApiCost> response) {
                ApiCost resp = response.body();

                if (resp.getStatus() == "false") {
                    new SweetAlertDialog(DeliveryDetailActivity2.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                } else {
                    List<Cost> grades = resp.getCosts();
                    List<String> strings = new ArrayList<String>();

                    for (int i = 0; i < grades.size(); i++) {
                        strings.add(grades.get(i).getCost_name());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(DeliveryDetailActivity2.this,
                            android.R.layout.simple_spinner_dropdown_item, strings);
                    spinner.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<ApiCost> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

}
