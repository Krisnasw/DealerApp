package com.luwakdev.dealer.delivery;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.luwakdev.dealer.R;
import com.luwakdev.dealer.barang.BarangAdapter;
import com.luwakdev.dealer.data.model.ApiSalesCost;
import com.luwakdev.dealer.data.model.Barang;
import com.luwakdev.dealer.data.model.SalesCostDetail;
import com.luwakdev.dealer.data.remote.ApiClient;
import com.luwakdev.dealer.util.AppConfig;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeliveryDetailAdapter extends RecyclerView.Adapter<DeliveryDetailAdapter.MyViewHolder> {

    Context context;
    private List<SalesCostDetail> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView title, stock, code, desc;

        public MyViewHolder(View view) {
            super(view);

            cv = (CardView) view.findViewById(R.id.cv);
            title = (TextView) view.findViewById(R.id.tvKeperluan);
            stock = (TextView) view.findViewById(R.id.tvBiaya);
            code = (TextView) view.findViewById(R.id.tvPeriode);
            desc = (TextView) view.findViewById(R.id.tvKeterangan);

        }

    }

    public DeliveryDetailAdapter(Context mainActivityContacts, List<SalesCostDetail> moviesList) {
        this.moviesList = moviesList;
        this.context = mainActivityContacts;
    }

    @Override
    public DeliveryDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_delivery_detail, parent, false);

        return new DeliveryDetailAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DeliveryDetailAdapter.MyViewHolder holder, int position) {
        final SalesCostDetail movie = moviesList.get(position);
        holder.title.setText("Keperluan : "+movie.getCost_name());
        holder.stock.setText("Total Biaya : "+movie.getSales_cost_detail_total());
        holder.code.setText("Tanggal Biaya : "+movie.getSales_cost_detail_date());
        holder.desc.setText("Keterangan : "+movie.getSales_cost_detail_desc());
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Info")
                        .setContentText("Apakah Anda Yakin Mengunci Item Ini")
                        .setConfirmText("Ya")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                lockItem(String.valueOf(movie.getSales_cost_detail_id()));
                            }
                        })
                        .setCancelText("Tidak")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private void lockItem(String code) {
        ApiClient.get(context).lockDetailCost(Prefs.getString(AppConfig.KEY_TOKEN, ""), code).enqueue(new Callback<ApiSalesCost>() {
            @Override
            public void onResponse(Call<ApiSalesCost> call, Response<ApiSalesCost> response) {
                ApiSalesCost resp = response.body();

                if (resp.getStatus() == "false") {
                    new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Info")
                            .setContentText(resp.getMessage())
                            .show();
                } else {
                    new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Info")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiSalesCost> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}