package com.luwakdev.dealer.delivery;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.luwakdev.dealer.R;
import com.luwakdev.dealer.data.model.SalesCost;
import com.luwakdev.dealer.data.model.SalesCostDetail;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class DeliveryAdapter extends RecyclerView.Adapter<DeliveryAdapter.MyViewHolder> {

    Context context;
    private List<SalesCost> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView title, stock, code, desc;

        public MyViewHolder(View view) {
            super(view);

            cv = (CardView) view.findViewById(R.id.cv);
            title = (TextView) view.findViewById(R.id.tvKeperluan);
            stock = (TextView) view.findViewById(R.id.tvBiaya);
            code = (TextView) view.findViewById(R.id.tvPeriode);
            desc = (TextView) view.findViewById(R.id.tvKeterangan);

        }

    }

    public DeliveryAdapter(Context mainActivityContacts, List<SalesCost> moviesList) {
        this.moviesList = moviesList;
        this.context = mainActivityContacts;
    }

    @Override
    public DeliveryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_delivery_detail, parent, false);

        return new DeliveryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DeliveryAdapter.MyViewHolder holder, int position) {
        final SalesCost movie = moviesList.get(position);
        holder.title.setText("Periode : " + movie.getSales_cost_start() + " - " + movie.getSales_cost_finish());
        holder.stock.setText("Modal Awal : " + movie.getSales_cost_modal());
        holder.code.setText("Nama Sales : " + movie.getSales_name());
        holder.desc.setText("Keterangan : " + movie.getSales_cost_desc());
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DeliveryDetailActivity2.class);
                i.putExtra("code", movie.getSales_cost_id());
                i.putExtra("periode", movie.getSales_cost_start() + " - " + movie.getSales_cost_finish());
                i.putExtra("modal", movie.getSales_cost_modal());
                i.putExtra("sales", movie.getSales_name());
                i.putExtra("keterangan", movie.getSales_cost_desc());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}