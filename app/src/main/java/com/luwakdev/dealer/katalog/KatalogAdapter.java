package com.luwakdev.dealer.katalog;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.luwakdev.dealer.R;
import com.luwakdev.dealer.data.model.Brand;
import com.luwakdev.dealer.data.model.Memo;
import com.luwakdev.dealer.data.model.Merk;
import com.luwakdev.dealer.memo.MemoAdapter;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class KatalogAdapter extends RecyclerView.Adapter<KatalogAdapter.MyViewHolder> {

    Context context;
    private List<Merk> brands;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvKatalog)
        TextView textView;
        @BindView(R.id.imgKatalog)
        ImageView imageView;
        @BindView(R.id.cardview)
        CardView cardView;

        Unbinder unbinder;

        public MyViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);
            unbinder = ButterKnife.bind(this, view);
        }

    }

    public KatalogAdapter(Context mainActivityContacts, List<Merk> brands) {
        this.brands = brands;
        this.context = mainActivityContacts;
    }

    @NonNull
    @Override
    public KatalogAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_katalog, parent, false);

        return new KatalogAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull KatalogAdapter.MyViewHolder holder, int position) {
        final Merk merk = brands.get(position);
        holder.textView.setText(merk.getBrand_name());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, KatalogDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("brand_id", String.valueOf(merk.getBrand_id()));
                bundle.putString("brand_name", merk.getBrand_name());
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_placeholder);
        requestOptions.error(R.drawable.ic_error);

        if (merk.getBrand_id() == 8) {
            Glide.with(context)
                    .load("https://images-na.ssl-images-amazon.com/images/I/51Tfykorz%2BL._SX425_.jpg")
                    .apply(requestOptions)
                    .into(holder.imageView);
        } else if (merk.getBrand_id() == 6) {
            Glide.with(context)
                    .load("https://www.seeklogo.net/wp-content/uploads/2013/03/honda-3d-vector-logo-400x400.png")
                    .apply(requestOptions)
                    .into(holder.imageView);
        } else if (merk.getBrand_id() == 9) {
            Glide.with(context)
                    .load("https://motorcycle-logos.com/wp-content/uploads/2016/10/Suzuki-logo.png")
                    .apply(requestOptions)
                    .into(holder.imageView);
        } else if (merk.getBrand_id() == 10) {
            Glide.with(context)
                    .load("https://seeklogo.com/images/K/kawasaki-logo-407E4B7736-seeklogo.com.png")
                    .apply(requestOptions)
                    .into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return brands.size();
    }
}