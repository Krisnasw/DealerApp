package com.luwakdev.dealer.katalog;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.luwakdev.dealer.R;
import com.luwakdev.dealer.data.model.BrandDetail;
import com.luwakdev.dealer.data.model.Merk;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class KatalogDetailAdapter extends RecyclerView.Adapter<KatalogDetailAdapter.MyViewHolder> {

    Context context;
    private List<BrandDetail> brandDetailList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvKatalog)
        TextView textView;
        @BindView(R.id.imgKatalog)
        ImageView imageView;
        @BindView(R.id.cardview)
        CardView cardView;

        Unbinder unbinder;

        public MyViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);
            unbinder = ButterKnife.bind(this, view);

        }

    }

    public KatalogDetailAdapter(Context mainActivityContacts, List<BrandDetail> brandDetailList) {
        this.brandDetailList = brandDetailList;
        this.context = mainActivityContacts;
    }

    @NonNull
    @Override
    public KatalogDetailAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_katalog, parent, false);

        return new KatalogDetailAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull KatalogDetailAdapter.MyViewHolder holder, int position) {
        final BrandDetail brandDetail = brandDetailList.get(position);

        holder.textView.setText(brandDetail.getBrand_detail_name());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, KatalogItemActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("brand_detail_id", String.valueOf(brandDetail.getBrand_detail_id()));
                bundle.putString("brand_detail_name", brandDetail.getBrand_detail_name());
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_placeholder);
        requestOptions.error(R.drawable.ic_error);

        if (brandDetail.getBrand_id() == String.valueOf(8)) {
            Glide.with(context)
                    .load("https://images-na.ssl-images-amazon.com/images/I/51Tfykorz%2BL._SX425_.jpg")
                    .apply(requestOptions)
                    .into(holder.imageView);
        } else if (brandDetail.getBrand_id() == String.valueOf(6)) {
            Glide.with(context)
                    .load("https://www.seeklogo.net/wp-content/uploads/2013/03/honda-3d-vector-logo-400x400.png")
                    .apply(requestOptions)
                    .into(holder.imageView);
        } else if (brandDetail.getBrand_id() == String.valueOf(9)) {
            Glide.with(context)
                    .load("https://motorcycle-logos.com/wp-content/uploads/2016/10/Suzuki-logo.png")
                    .apply(requestOptions)
                    .into(holder.imageView);
        } else if (brandDetail.getBrand_id() == String.valueOf(10)) {
            Glide.with(context)
                    .load("https://seeklogo.com/images/K/kawasaki-logo-407E4B7736-seeklogo.com.png")
                    .apply(requestOptions)
                    .into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return brandDetailList.size();
    }
}
