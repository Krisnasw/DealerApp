package com.luwakdev.dealer.katalog;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import com.luwakdev.dealer.R;
import com.luwakdev.dealer.data.model.ApiBrandDetail;
import com.luwakdev.dealer.data.model.ApiMerk;
import com.luwakdev.dealer.data.model.BrandDetail;
import com.luwakdev.dealer.data.model.Merk;
import com.luwakdev.dealer.data.remote.ApiClient;
import com.luwakdev.dealer.util.AppConfig;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KatalogDetailActivity extends AppCompatActivity {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    Unbinder unbinder;

    SweetAlertDialog sweetAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_katalog);

        ButterKnife.bind(this);
        unbinder = ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Katalog Detail");

        sweetAlertDialog = new SweetAlertDialog(KatalogDetailActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.setTitleText("Loading");
        sweetAlertDialog.setContentText("Please wait...");
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.show();

        getBrandDetail();
    }

    protected void getBrandDetail() {
        ApiClient.get(this).getDetailById(Prefs.getString(AppConfig.KEY_TOKEN, ""), Integer.parseInt(getIntent().getExtras().getString("brand_id"))).enqueue(new Callback<ApiBrandDetail>() {
            @Override
            public void onResponse(Call<ApiBrandDetail> call, Response<ApiBrandDetail> response) {
                ApiBrandDetail resp = response.body();

                if (resp.getStatus().equals("success")) {
                    sweetAlertDialog.dismissWithAnimation();
                    List<BrandDetail> barangs = resp.getBrandDetailList();

                    recyclerView.setVisibility(View.VISIBLE);
                    LinearLayoutManager linearVertical = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);

                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(linearVertical);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(new KatalogDetailAdapter(KatalogDetailActivity.this, barangs));
                } else {
                    sweetAlertDialog.dismissWithAnimation();
                    recyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ApiBrandDetail> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
