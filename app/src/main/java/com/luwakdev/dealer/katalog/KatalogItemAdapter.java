package com.luwakdev.dealer.katalog;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.luwakdev.dealer.R;
import com.luwakdev.dealer.data.model.Barang;
import com.luwakdev.dealer.data.model.BrandDetail;
import com.luwakdev.dealer.data.model.Merk;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class KatalogItemAdapter extends RecyclerView.Adapter<KatalogItemAdapter.MyViewHolder> {

    Context context;
    private List<Barang> barangs;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvKatalog)
        TextView textView;
        @BindView(R.id.imgKatalog)
        ImageView imageView;
        @BindView(R.id.cardview)
        CardView cardView;

        Unbinder unbinder;

        public MyViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);
            unbinder = ButterKnife.bind(this, view);
        }

    }

    public KatalogItemAdapter(Context mainActivityContacts, List<Barang> barangs) {
        this.barangs = barangs;
        this.context = mainActivityContacts;
    }

    @NonNull
    @Override
    public KatalogItemAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_katalog, parent, false);

        return new KatalogItemAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull KatalogItemAdapter.MyViewHolder holder, int position) {
        final Barang brandDetail = barangs.get(position);

        holder.textView.setText(brandDetail.getItem_code() + " - " + brandDetail.getItem_name());

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_placeholder);
        requestOptions.error(R.drawable.ic_error);

        Glide.with(context)
                .applyDefaultRequestOptions(requestOptions)
                .load("http://sparepartexcavator.com/wp-content/uploads/2017/07/cropped-icon-sparepart.png")
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return barangs.size();
    }

}
