package com.luwakdev.dealer.barang;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.luwakdev.dealer.R;
import com.luwakdev.dealer.data.model.ApiBarang;
import com.luwakdev.dealer.data.model.Barang;
import com.luwakdev.dealer.data.remote.ApiClient;
import com.luwakdev.dealer.util.AppConfig;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BarangActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.imgNotFound)
    ImageView notFound;
    @BindView(R.id.tvNotFound)
    TextView textView;

    private BarangAdapter barangAdapter;
    SweetAlertDialog swal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barang);

        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        swal = new SweetAlertDialog(BarangActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        swal.setTitleText("Loading");
        swal.setContentText("Getting data...");
        swal.setCancelable(false);
        swal.show();

        getListItem();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.length() > 3) {
            getSearchBarang(newText);
            recyclerView.setAdapter(barangAdapter);
        }
        return true;
    }

    private void getListItem() {
        ApiClient.get(this).getListBarang(Prefs.getString(AppConfig.KEY_TOKEN, "")).enqueue(new Callback<ApiBarang>() {
            @Override
            public void onResponse(Call<ApiBarang> call, Response<ApiBarang> response) {
                ApiBarang resp = response.body();
                if (resp.getStatus() == "false") {
                    swal.dismissWithAnimation();
                    recyclerView.setVisibility(View.GONE);
                    notFound.setVisibility(View.VISIBLE);
                    textView.setVisibility(View.VISIBLE);
                } else {
                    swal.dismissWithAnimation();
                    List<Barang> barangs = resp.getBarangs();

                    if (barangs.size() == 0) {
                        recyclerView.setVisibility(View.GONE);
                        notFound.setVisibility(View.VISIBLE);
                        textView.setVisibility(View.VISIBLE);
                    }

                    LinearLayoutManager linearVertical = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);

                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(linearVertical);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(new BarangAdapter(BarangActivity.this, barangs));

                }
            }

            @Override
            public void onFailure(Call<ApiBarang> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getSearchBarang(String title) {
        ApiClient.get(this).getSearchBarang(title, Prefs.getString(AppConfig.KEY_TOKEN, "")).enqueue(new Callback<ApiBarang>() {
            @Override
            public void onResponse(Call<ApiBarang> call, Response<ApiBarang> response) {
                ApiBarang resp = response.body();
                if (resp.getStatus() == "false") {
                    swal.dismissWithAnimation();
                    recyclerView.setVisibility(View.GONE);
                    notFound.setVisibility(View.VISIBLE);
                    textView.setVisibility(View.VISIBLE);

                } else {
                    swal.dismissWithAnimation();
                    List<Barang> barangs = resp.getBarangs();

                    if (barangs.size() == 0) {
                        recyclerView.setVisibility(View.GONE);
                        notFound.setVisibility(View.VISIBLE);
                        textView.setVisibility(View.VISIBLE);
                    }

                    LinearLayoutManager linearVertical = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);

                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(linearVertical);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(new BarangAdapter(BarangActivity.this, barangs));

                }
            }

            @Override
            public void onFailure(Call<ApiBarang> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
