package com.luwakdev.dealer.barang;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.luwakdev.dealer.R;
import com.luwakdev.dealer.data.model.Barang;
import com.luwakdev.dealer.data.model.HomeGridModelClass;
import com.luwakdev.dealer.promo.PromoAdapter;

import java.util.List;

import butterknife.ButterKnife;

public class BarangAdapter extends RecyclerView.Adapter<BarangAdapter.MyViewHolder> {

    Context context;
    private List<Barang> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView title, stock, code;

        public MyViewHolder(View view) {
            super(view);

            cv = (CardView) view.findViewById(R.id.cv);
            title = (TextView) view.findViewById(R.id.namaBarang);
            stock = (TextView) view.findViewById(R.id.stokBarang);
            code = (TextView) view.findViewById(R.id.kodeBarang);

        }

    }

    public BarangAdapter(Context mainActivityContacts, List<Barang> moviesList) {
        this.moviesList = moviesList;
        this.context = mainActivityContacts;
    }

    @Override
    public BarangAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_barang, parent, false);

        return new BarangAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final BarangAdapter.MyViewHolder holder, int position) {
        Barang movie = moviesList.get(position);
        holder.title.setText(movie.getItem_name().trim());
        holder.stock.setText(movie.getItem_stock());
        holder.code.setText(movie.getItem_code());
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}