package com.luwakdev.dealer.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesCost {

    @SerializedName("sales_cost_id")
    @Expose
    private String sales_cost_id;
    @SerializedName("sales_cost_start")
    @Expose
    private String sales_cost_start;
    @SerializedName("sales_cost_finish")
    @Expose
    private String sales_cost_finish;
    @SerializedName("sales_cost_modal")
    @Expose
    private String sales_cost_modal;
    @SerializedName("sales_cost_desc")
    @Expose
    private String sales_cost_desc;
    @SerializedName("sales_name")
    @Expose
    private String sales_name;

    public SalesCost(String sales_cost_id, String sales_cost_start, String sales_cost_finish, String sales_cost_modal, String sales_cost_desc, String sales_name) {
        this.sales_cost_id = sales_cost_id;
        this.sales_cost_start = sales_cost_start;
        this.sales_cost_finish = sales_cost_finish;
        this.sales_cost_modal = sales_cost_modal;
        this.sales_cost_desc = sales_cost_desc;
        this.sales_name = sales_name;
    }

    public String getSales_cost_id() {
        return sales_cost_id;
    }

    public void setSales_cost_id(String sales_cost_id) {
        this.sales_cost_id = sales_cost_id;
    }

    public String getSales_cost_start() {
        return sales_cost_start;
    }

    public void setSales_cost_start(String sales_cost_start) {
        this.sales_cost_start = sales_cost_start;
    }

    public String getSales_cost_finish() {
        return sales_cost_finish;
    }

    public void setSales_cost_finish(String sales_cost_finish) {
        this.sales_cost_finish = sales_cost_finish;
    }

    public String getSales_cost_modal() {
        return sales_cost_modal;
    }

    public void setSales_cost_modal(String sales_cost_modal) {
        this.sales_cost_modal = sales_cost_modal;
    }

    public String getSales_cost_desc() {
        return sales_cost_desc;
    }

    public void setSales_cost_desc(String sales_cost_desc) {
        this.sales_cost_desc = sales_cost_desc;
    }

    public String getSales_name() {
        return sales_name;
    }

    public void setSales_name(String sales_name) {
        this.sales_name = sales_name;
    }
}
