package com.luwakdev.dealer.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sales {

    @SerializedName("sales_id")
    @Expose
    private Integer sales_id;
    @SerializedName("sales_name")
    @Expose
    private String sales_name;

    public Sales(Integer sales_id, String sales_name) {
        this.sales_id = sales_id;
        this.sales_name = sales_name;
    }

    public Integer getSales_id() {
        return sales_id;
    }

    public void setSales_id(Integer sales_id) {
        this.sales_id = sales_id;
    }

    public String getSales_name() {
        return sales_name;
    }

    public void setSales_name(String sales_name) {
        this.sales_name = sales_name;
    }
}
