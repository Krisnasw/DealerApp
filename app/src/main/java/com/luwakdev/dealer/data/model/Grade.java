package com.luwakdev.dealer.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Grade {

    @SerializedName("grade_id")
    @Expose
    private Integer grade_id;
    @SerializedName("grade_nama")
    @Expose
    private String grade_nama;

    public Grade(Integer grade_id, String grade_nama) {
        this.grade_id = grade_id;
        this.grade_nama = grade_nama;
    }

    public Integer getGrade_id() {
        return grade_id;
    }

    public void setGrade_id(Integer grade_id) {
        this.grade_id = grade_id;
    }

    public String getGrade_nama() {
        return grade_nama;
    }

    public void setGrade_nama(String grade_nama) {
        this.grade_nama = grade_nama;
    }
}
