package com.luwakdev.dealer.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Barang {

    @SerializedName("item_name")
    @Expose
    String item_name;
    @SerializedName("item_stock")
    @Expose
    String item_stock;
    @SerializedName("item_code")
    @Expose
    String item_code;

    public Barang(String item_name, String item_stock, String item_code) {
        this.item_name = item_name;
        this.item_stock = item_stock;
        this.item_code = item_code;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_stock() {
        return item_stock;
    }

    public void setItem_stock(String item_stock) {
        this.item_stock = item_stock;
    }

    public String getItem_code() {
        return item_code;
    }

    public void setItem_code(String item_code) {
        this.item_code = item_code;
    }
}
