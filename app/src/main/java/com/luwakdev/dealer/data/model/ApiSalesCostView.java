package com.luwakdev.dealer.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiSalesCostView {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<SalesCost> salesCosts;

    public ApiSalesCostView(String status, String message, List<SalesCost> salesCosts) {
        this.status = status;
        this.message = message;
        this.salesCosts = salesCosts;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SalesCost> getSalesCosts() {
        return salesCosts;
    }

    public void setSalesCosts(List<SalesCost> salesCosts) {
        this.salesCosts = salesCosts;
    }
}
