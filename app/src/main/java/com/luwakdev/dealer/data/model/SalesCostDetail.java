package com.luwakdev.dealer.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesCostDetail {

    @SerializedName("sales_cost_detail_id")
    @Expose
    private Integer sales_cost_detail_id;
    @SerializedName("cost_name")
    @Expose
    private String cost_name;
    @SerializedName("sales_cost_detail_date")
    @Expose
    private String sales_cost_detail_date;
    @SerializedName("sales_cost_detail_total")
    @Expose
    private String sales_cost_detail_total;
    @SerializedName("sales_cost_detail_desc")
    @Expose
    private String sales_cost_detail_desc;

    public SalesCostDetail(Integer sales_cost_detail_id, String cost_name, String sales_cost_detail_date, String sales_cost_detail_total, String sales_cost_detail_desc) {
        this.sales_cost_detail_id = sales_cost_detail_id;
        this.cost_name = cost_name;
        this.sales_cost_detail_date = sales_cost_detail_date;
        this.sales_cost_detail_total = sales_cost_detail_total;
        this.sales_cost_detail_desc = sales_cost_detail_desc;
    }

    public Integer getSales_cost_detail_id() {
        return sales_cost_detail_id;
    }

    public void setSales_cost_detail_id(Integer sales_cost_detail_id) {
        this.sales_cost_detail_id = sales_cost_detail_id;
    }

    public String getCost_name() {
        return cost_name;
    }

    public void setCost_name(String cost_name) {
        this.cost_name = cost_name;
    }

    public String getSales_cost_detail_date() {
        return sales_cost_detail_date;
    }

    public void setSales_cost_detail_date(String sales_cost_detail_date) {
        this.sales_cost_detail_date = sales_cost_detail_date;
    }

    public String getSales_cost_detail_total() {
        return sales_cost_detail_total;
    }

    public void setSales_cost_detail_total(String sales_cost_detail_total) {
        this.sales_cost_detail_total = sales_cost_detail_total;
    }

    public String getSales_cost_detail_desc() {
        return sales_cost_detail_desc;
    }

    public void setSales_cost_detail_desc(String sales_cost_detail_desc) {
        this.sales_cost_detail_desc = sales_cost_detail_desc;
    }
}