package com.luwakdev.dealer.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiSalesCostGet {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("sales")
    @Expose
    private String sales;
    @SerializedName("keterangan")
    @Expose
    private String keterangan;
    @SerializedName("modal")
    @Expose
    private Integer modal;
    @SerializedName("periode")
    @Expose
    private String periode;
    @SerializedName("data")
    @Expose
    private List<SalesCostDetail> salesCostDetailList;

    public ApiSalesCostGet(String status, String message, String sales, String keterangan, Integer modal, String periode, List<SalesCostDetail> salesCostDetailList) {
        this.status = status;
        this.message = message;
        this.sales = sales;
        this.keterangan = keterangan;
        this.modal = modal;
        this.periode = periode;
        this.salesCostDetailList = salesCostDetailList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSales() {
        return sales;
    }

    public void setSales(String sales) {
        this.sales = sales;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public Integer getModal() {
        return modal;
    }

    public void setModal(Integer modal) {
        this.modal = modal;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public List<SalesCostDetail> getSalesCostDetailList() {
        return salesCostDetailList;
    }

    public void setSalesCostDetailList(List<SalesCostDetail> salesCostDetailList) {
        this.salesCostDetailList = salesCostDetailList;
    }
}
