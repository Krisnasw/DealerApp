package com.luwakdev.dealer.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiMerk {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Merk> merkList;

    public ApiMerk(String status, String message, List<Merk> merkList) {
        this.status = status;
        this.message = message;
        this.merkList = merkList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Merk> getMerkList() {
        return merkList;
    }

    public void setMerkList(List<Merk> merkList) {
        this.merkList = merkList;
    }
}
