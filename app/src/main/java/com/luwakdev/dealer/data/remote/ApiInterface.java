package com.luwakdev.dealer.data.remote;

import com.luwakdev.dealer.data.model.ApiBarang;
import com.luwakdev.dealer.data.model.ApiBrandDetail;
import com.luwakdev.dealer.data.model.ApiCardboard;
import com.luwakdev.dealer.data.model.ApiCity;
import com.luwakdev.dealer.data.model.ApiCost;
import com.luwakdev.dealer.data.model.ApiCustomer;
import com.luwakdev.dealer.data.model.ApiGrade;
import com.luwakdev.dealer.data.model.ApiListCustomer;
import com.luwakdev.dealer.data.model.ApiListMemoDetail;
import com.luwakdev.dealer.data.model.ApiLogin;
import com.luwakdev.dealer.data.model.ApiMemo;
import com.luwakdev.dealer.data.model.ApiMemoDetail;
import com.luwakdev.dealer.data.model.ApiMerk;
import com.luwakdev.dealer.data.model.ApiPromo;
import com.luwakdev.dealer.data.model.ApiSales;
import com.luwakdev.dealer.data.model.ApiSalesCost;
import com.luwakdev.dealer.data.model.ApiSalesCostGet;
import com.luwakdev.dealer.data.model.ApiSalesCostView;
import com.luwakdev.dealer.data.model.ApiTracking;
import com.luwakdev.dealer.data.model.ApiUser;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("auth/login")
    Call<ApiLogin> login(@Field("user_username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("auth/user")
    Call<ApiUser> getUserData(@Field("token") String token);

    @FormUrlEncoded
    @POST("list/barang")
    Call<ApiBarang> getListBarang(@Field("token") String token);

    @FormUrlEncoded
    @POST("list/barang/search")
    Call<ApiBarang> getSearchBarang(@Field("title") String title, @Field("token") String token);

    @FormUrlEncoded
    @POST("list/grade")
    Call<ApiGrade> getGrade(@Field("token") String token);

    @Multipart
    @POST("customer/add")
    Call<ApiCustomer> createCustomer(@Part("token") RequestBody token, @Part("customerCode") RequestBody customerCode, @Part("customerName") RequestBody customerName,
                                     @Part("customerToko") RequestBody customerToko, @Part("customerAddress") RequestBody customerAddress, @Part("customerPhone") RequestBody customerPhone,
                                     @Part("customerEmail") RequestBody customerEmail, @Part("customerGrade") RequestBody customerGrade, @Part("customerRekening") RequestBody customerRekening,
                                     @Part("customerBank") RequestBody customerBank, @Part("customerNPWP") RequestBody customerNPWP, @Part("customerNpwpName") RequestBody customerNpwpName,
                                     @Part("customerEkspedisi") RequestBody customerEkspedisi, @Part("customerHp") RequestBody customerHp, @Part("customerFax") RequestBody customerFax,
                                     @Part("customerWilayah") RequestBody customerWilayah, @Part("customerLat") RequestBody customerLat, @Part("customerLong") RequestBody customerLong,
                                     @Part MultipartBody.Part customerImg);

    @FormUrlEncoded
    @POST("list/city")
    Call<ApiCity> getCity(@Field("token") String token);

    @FormUrlEncoded
    @POST("list/customer")
    Call<ApiListCustomer> getListCustomer(@Field("token") String token);

    @FormUrlEncoded
    @POST("list/merk")
    Call<ApiMerk> getListMerk(@Field("token") String token);

    @FormUrlEncoded
    @POST("list/sales")
    Call<ApiSales> getListSales(@Field("token") String token);

    @FormUrlEncoded
    @POST("memo-detail/add")
    Call<ApiMemoDetail> createMemoDetail(@Field("token") String token, @Field("item") String item, @Field("qty") String qty, @Field("diskon") String diskon);

    @FormUrlEncoded
    @POST("list/memo-detail")
    Call<ApiListMemoDetail> getListMemoDetail(@Field("token") String token);

    @FormUrlEncoded
    @POST("memo/add")
    Call<ApiMemo> createMemo(@Field("token") String token, @Field("customer") String customer, @Field("merk") String merk, @Field("sales") String sales);

    @FormUrlEncoded
    @POST("cardboard/check")
    Call<ApiCardboard> cekBarcode(@Field("token") String token, @Field("code") String code);

    @FormUrlEncoded
    @POST("cardboard/add")
    Call<ApiCardboard> postCardboard(@Field("token") String token, @Field("code") String code, @Field("type") String type);

    @FormUrlEncoded
    @POST("list/promo")
    Call<ApiPromo> getPromoData(@Field("token") String token);

    @FormUrlEncoded
    @POST("memo-detail/delete")
    Call<ApiMemo> deleteMemoDetail(@Field("token") String token, @Field("code") Integer code);

    @FormUrlEncoded
    @POST("memo-detail/update")
    Call<ApiMemo> updateMemoDetail(@Field("token") String token, @Field("code") Integer code, @Field("qty") Integer qty);

    @FormUrlEncoded
    @POST("tracking")
    Call<ApiTracking> getTrackingDetail(@Field("token") String token, @Field("nota") String nota);

    @FormUrlEncoded
    @POST("sales-cost/add")
    Call<ApiSalesCost> postBiaya(@Field("token") String token, @Field("date") String date, @Field("modal") String modal, @Field("sales") String sales, @Field("keterangan") String keterangan);

    @FormUrlEncoded
    @POST("list/keperluan")
    Call<ApiCost> getKeperluan(@Field("token") String token);

    @FormUrlEncoded
    @POST("list/sales-cost")
    Call<ApiSalesCostGet> getSalesCostData(@Field("token") String token, @Field("code") Integer code);

    @FormUrlEncoded
    @POST("sales-cost/detail/add")
    Call<ApiSalesCost> postCostDetail(@Field("token") String token, @Field("code") Integer code, @Field("sales") String sales, @Field("cost") String cost, @Field("date") String date, @Field("total") String total, @Field("desc") String desc);

    @FormUrlEncoded
    @POST("list/sales-cost/sales")
    Call<ApiSalesCostView> getSalesCostDetailBySales(@Field("token") String token, @Field("sales") String sales);

    @FormUrlEncoded
    @POST("sales-cost/detail/lock")
    Call<ApiSalesCost> lockDetailCost(@Field("token") String token, @Field("code") String code);

    @FormUrlEncoded
    @POST("sales-cost/lock")
    Call<ApiSalesCost> lockSalesCost(@Field("token") String token, @Field("id") Integer id, @Field("less") String less);

    @FormUrlEncoded
    @POST("list/brand-detail")
    Call<ApiBrandDetail> getDetailById(@Field("token") String token, @Field("id") Integer id);

    @FormUrlEncoded
    @POST("list/item-detail")
    Call<ApiBarang> getItemByDetail(@Field("token") String token, @Field("id") Integer id);
}