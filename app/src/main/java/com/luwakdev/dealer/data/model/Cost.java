package com.luwakdev.dealer.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cost {

    @SerializedName("cost_id")
    @Expose
    private Integer cost_id;
    @SerializedName("cost_name")
    @Expose
    private String cost_name;

    public Integer getCost_id() {
        return cost_id;
    }

    public void setCost_id(Integer cost_id) {
        this.cost_id = cost_id;
    }

    public String getCost_name() {
        return cost_name;
    }

    public void setCost_name(String cost_name) {
        this.cost_name = cost_name;
    }

    public Cost(Integer cost_id, String cost_name) {
        this.cost_id = cost_id;
        this.cost_name = cost_name;
    }
}
