package com.luwakdev.dealer.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Memo {

    @SerializedName("memo_detail_id")
    @Expose
    private Integer memo_detail_id;
    @SerializedName("memo_detail_discount")
    @Expose
    private Integer memo_detail_discount;
    @SerializedName("memo_detail_qty")
    @Expose
    private Integer memo_detail_qty;
    @SerializedName("item_name")
    @Expose
    private String item_name;
    @SerializedName("memo_accumulation")
    @Expose
    private String memo_accumulation;

    public Memo(Integer memo_detail_id, Integer memo_detail_discount, Integer memo_detail_qty, String item_name, String memo_accumulation) {
        this.memo_detail_id = memo_detail_id;
        this.memo_detail_discount = memo_detail_discount;
        this.memo_detail_qty = memo_detail_qty;
        this.item_name = item_name;
        this.memo_accumulation = memo_accumulation;
    }

    public Integer getMemo_detail_id() {
        return memo_detail_id;
    }

    public void setMemo_detail_id(Integer memo_detail_id) {
        this.memo_detail_id = memo_detail_id;
    }

    public Integer getMemo_detail_discount() {
        return memo_detail_discount;
    }

    public void setMemo_detail_discount(Integer memo_detail_discount) {
        this.memo_detail_discount = memo_detail_discount;
    }

    public Integer getMemo_detail_qty() {
        return memo_detail_qty;
    }

    public void setMemo_detail_qty(Integer memo_detail_qty) {
        this.memo_detail_qty = memo_detail_qty;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getMemo_accumulation() {
        return memo_accumulation;
    }

    public void setMemo_accumulation(String memo_accumulation) {
        this.memo_accumulation = memo_accumulation;
    }
}
