package com.luwakdev.dealer.data.model;

public class User {

    private Integer user_id,user_type_id,employee_id;
    private String user_username, password, user_first_name, user_last_name, user_email, user_phone, user_img, user_active_status;

    public User(Integer user_id, Integer user_type_id, Integer employee_id, String user_username, String password, String user_first_name, String user_last_name,
                String user_email, String user_phone, String user_img, String user_active_status) {
        this.user_id = user_id;
        this.user_type_id = user_type_id;
        this.employee_id = employee_id;
        this.user_username = user_username;
        this.password = password;
        this.user_first_name = user_first_name;
        this.user_last_name = user_last_name;
        this.user_email = user_email;
        this.user_phone = user_phone;
        this.user_img = user_img;
        this.user_active_status = user_active_status;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getUser_type_id() {
        return user_type_id;
    }

    public void setUser_type_id(Integer user_type_id) {
        this.user_type_id = user_type_id;
    }

    public Integer getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(Integer employee_id) {
        this.employee_id = employee_id;
    }

    public String getUser_username() {
        return user_username;
    }

    public void setUser_username(String user_username) {
        this.user_username = user_username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUser_first_name() {
        return user_first_name;
    }

    public void setUser_first_name(String user_first_name) {
        this.user_first_name = user_first_name;
    }

    public String getUser_last_name() {
        return user_last_name;
    }

    public void setUser_last_name(String user_last_name) {
        this.user_last_name = user_last_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getUser_img() {
        return user_img;
    }

    public void setUser_img(String user_img) {
        this.user_img = user_img;
    }

    public String getUser_active_status() {
        return user_active_status;
    }

    public void setUser_active_status(String user_active_status) {
        this.user_active_status = user_active_status;
    }

}