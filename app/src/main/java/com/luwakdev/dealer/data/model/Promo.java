package com.luwakdev.dealer.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Promo {

    @SerializedName("bonus_id")
    @Expose
    private Integer bonus_id;
    @SerializedName("bonus_name")
    @Expose
    private String bonus_name;

    public Promo(Integer bonus_id, String bonus_name) {
        this.bonus_id = bonus_id;
        this.bonus_name = bonus_name;
    }

    public Integer getBonus_id() {
        return bonus_id;
    }

    public void setBonus_id(Integer bonus_id) {
        this.bonus_id = bonus_id;
    }

    public String getBonus_name() {
        return bonus_name;
    }

    public void setBonus_name(String bonus_name) {
        this.bonus_name = bonus_name;
    }
}
