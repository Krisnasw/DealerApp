package com.luwakdev.dealer.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Brand {

    @SerializedName("brand_id")
    @Expose
    private int brand_id;
    @SerializedName("brand_code")
    @Expose
    private String brand_code;
    @SerializedName("brand_name")
    @Expose
    private String brand_name;

    public Brand(int brand_id, String brand_code, String brand_name) {
        this.brand_id = brand_id;
        this.brand_code = brand_code;
        this.brand_name = brand_name;
    }

    public int getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(int brand_id) {
        this.brand_id = brand_id;
    }

    public String getBrand_code() {
        return brand_code;
    }

    public void setBrand_code(String brand_code) {
        this.brand_code = brand_code;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }
}
