package com.luwakdev.dealer.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiBrand {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Brand> barangs;

    public ApiBrand(String status, String message, List<Brand> barangs) {
        this.status = status;
        this.message = message;
        this.barangs = barangs;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Brand> getBarangs() {
        return barangs;
    }

    public void setBarangs(List<Brand> barangs) {
        this.barangs = barangs;
    }

}
