package com.luwakdev.dealer.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BrandDetail {

    @SerializedName("brand_detail_id")
    @Expose
    private Integer brand_detail_id;
    @SerializedName("brand_detail_name")
    @Expose
    private String brand_detail_name;
    @SerializedName("brand_id")
    @Expose
    private String brand_id;

    public BrandDetail(Integer brand_detail_id, String brand_detail_name, String brand_id) {
        this.brand_detail_id = brand_detail_id;
        this.brand_detail_name = brand_detail_name;
        this.brand_id = brand_id;
    }

    public Integer getBrand_detail_id() {
        return brand_detail_id;
    }

    public void setBrand_detail_id(Integer brand_detail_id) {
        this.brand_detail_id = brand_detail_id;
    }

    public String getBrand_detail_name() {
        return brand_detail_name;
    }

    public void setBrand_detail_name(String brand_detail_name) {
        this.brand_detail_name = brand_detail_name;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }
}
