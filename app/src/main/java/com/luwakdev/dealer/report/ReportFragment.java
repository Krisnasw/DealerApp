package com.luwakdev.dealer.report;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.luwakdev.dealer.R;
import com.luwakdev.dealer.data.model.ApiTracking;
import com.luwakdev.dealer.data.remote.ApiClient;
import com.luwakdev.dealer.util.AppConfig;
import com.pixplicity.easyprefs.library.Prefs;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.Calendar;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportFragment extends Fragment {

    @BindView(R.id.startDate)
    MaterialEditText startDate;
    @BindView(R.id.btn_id)
    CircularProgressButton btnSubmit;
    @BindView(R.id.cv)
    CardView cardView;
    @BindView(R.id.nota)
    TextView nota;
    @BindView(R.id.users)
    TextView users;
    @BindView(R.id.cardboard)
    TextView cardboard;
    @BindView(R.id.tglPacking)
    TextView tglPacking;
    @BindView(R.id.status)
    TextView status;

    public ReportFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report, container, false);

        ButterKnife.bind(this, view);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardView.setVisibility(View.GONE);
                btnSubmit.setGravity(Gravity.CENTER);
                btnSubmit.startAnimation();
                getTracking();
            }
        });

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        btnSubmit.dispose();
    }

    private void getTracking() {
        ApiClient.get(getActivity()).getTrackingDetail(Prefs.getString(AppConfig.KEY_TOKEN, ""), startDate.getText().toString()).enqueue(new Callback<ApiTracking>() {
            @Override
            public void onResponse(Call<ApiTracking> call, Response<ApiTracking> response) {
                ApiTracking resp = response.body();
                if (resp.getStatus() == "false") {
                    btnSubmit.revertAnimation();
                    btnSubmit.dispose();
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                } else {
                    btnSubmit.revertAnimation();
                    btnSubmit.dispose();
                    cardView.setVisibility(View.VISIBLE);
                    nota.setText("Nomor Nota : "+startDate.getText().toString());
                    status.setText("Status : "+resp.getData());
                    tglPacking.setText("Tanggal Packing : "+resp.getDate());
                    users.setText("Nama User : "+resp.getUser());
                    cardboard.setText("Kode Packing : "+resp.getPacking());
                }
            }

            @Override
            public void onFailure(Call<ApiTracking> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Konek Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
