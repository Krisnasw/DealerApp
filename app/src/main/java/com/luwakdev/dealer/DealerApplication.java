package com.luwakdev.dealer;

import android.app.Application;
import android.content.ContextWrapper;
import android.widget.Toast;

import com.luwakdev.dealer.util.NetworkUtil;
import com.pixplicity.easyprefs.library.Prefs;

public class DealerApplication extends Application {

    public void onCreate() {
        super.onCreate();

        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();

        if (NetworkUtil.getInstance(this).isOnline()) {

        } else {
            Toast.makeText(getApplicationContext(), "Tidak Ada Koneksi Internet", Toast.LENGTH_SHORT).show();
        }
    }

}
