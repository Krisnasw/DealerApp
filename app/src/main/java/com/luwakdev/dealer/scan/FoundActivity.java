package com.luwakdev.dealer.scan;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.luwakdev.dealer.R;
import com.luwakdev.dealer.data.model.ApiCardboard;
import com.luwakdev.dealer.data.remote.ApiClient;
import com.luwakdev.dealer.main.MainActivity;
import com.luwakdev.dealer.util.AppConfig;
import com.pixplicity.easyprefs.library.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FoundActivity extends AppCompatActivity {

    @BindView(R.id.btnAccept)
    Button btnAcc;
    @BindView(R.id.btnCancel)
    Button btnCancel;
    @BindView(R.id.no)
    TextView nomer;

    private SweetAlertDialog swal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_found);

        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle b = new Bundle();
        b = getIntent().getExtras();
        final String codes = b.getString("code");

        nomer.setText("No Barcode : "+codes);

        btnAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swal = new SweetAlertDialog(FoundActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                swal.setTitleText("Loading");
                swal.setContentText("Processing...");
                swal.setCancelable(false);
                swal.show();
                postCardboardStatus(codes,"Terima");
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swal = new SweetAlertDialog(FoundActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                swal.setTitleText("Loading");
                swal.setContentText("Processing...");
                swal.setCancelable(false);
                swal.show();
                postCardboardStatus(codes,"Tolak");
            }
        });
    }

    protected void postCardboardStatus(String code, String type) {
        ApiClient.get(this).postCardboard(Prefs.getString(AppConfig.KEY_TOKEN, ""), code, type).enqueue(new Callback<ApiCardboard>() {
            @Override
            public void onResponse(Call<ApiCardboard> call, Response<ApiCardboard> response) {
                ApiCardboard resp = response.body();

                if (resp.getStatus() == "false") {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(FoundActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                } else {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(FoundActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    startActivity(new Intent(FoundActivity.this, MainActivity.class));
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiCardboard> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
