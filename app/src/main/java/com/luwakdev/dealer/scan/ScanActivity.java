package com.luwakdev.dealer.scan;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.zxing.Result;
import com.luwakdev.dealer.R;
import com.luwakdev.dealer.data.model.ApiCardboard;
import com.luwakdev.dealer.data.remote.ApiClient;
import com.luwakdev.dealer.util.AppConfig;
import com.pixplicity.easyprefs.library.Prefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    @BindView(R.id.frameScan)
    FrameLayout frameLayout;

    private ZXingScannerView mScannerView;
    private boolean isCaptured = false;

    private SweetAlertDialog swal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Scan Barcode");

        ButterKnife.bind(this);

        initScannerView();
    }

    private void initScannerView() {
        mScannerView = new ZXingScannerView(this);
        mScannerView.setAutoFocus(true);
        mScannerView.setResultHandler(this);
        frameLayout.addView(mScannerView);
    }

    @Override
    public void handleResult(Result result) {
        Toast.makeText(this, ""+result.getText(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        mScannerView.startCamera();
        doRequestPermission();
        super.onStart();
    }

    private void doRequestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 100);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //granted
                    mScannerView.startCamera();
                } else {
                    //not granted
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onPause() {
        mScannerView.stopCamera();
        super.onPause();
    }

    private void cekBarcode() {
        ApiClient.get(this).cekBarcode(Prefs.getString(AppConfig.KEY_TOKEN, ""), "").enqueue(new Callback<ApiCardboard>() {
            @Override
            public void onResponse(Call<ApiCardboard> call, Response<ApiCardboard> response) {
                ApiCardboard resp = response.body();

                if (resp.getStatus() == "false") {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(ScanActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                } else {
                    swal.dismissWithAnimation();
                    Intent intent=new Intent(ScanActivity.this, FoundActivity.class);
                    intent.putExtra("code", 0);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<ApiCardboard> call, Throwable t) {
                try {
                    swal.dismissWithAnimation();
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}