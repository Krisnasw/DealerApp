package com.luwakdev.dealer.customer;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.luwakdev.dealer.R;
import com.luwakdev.dealer.data.model.ApiCity;
import com.luwakdev.dealer.data.model.ApiCustomer;
import com.luwakdev.dealer.data.model.ApiGrade;
import com.luwakdev.dealer.data.model.City;
import com.luwakdev.dealer.data.model.Grade;
import com.luwakdev.dealer.data.remote.ApiClient;
import com.luwakdev.dealer.main.MainActivity;
import com.luwakdev.dealer.util.AppConfig;
import com.luwakdev.dealer.util.Tracker;
import com.pixplicity.easyprefs.library.Prefs;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerActivity extends AppCompatActivity implements GoogleMap.OnMarkerClickListener, OnMapReadyCallback {

    @BindView(R.id.customerCode)
    MaterialEditText customerCode;
    @BindView(R.id.customerName)
    MaterialEditText customerName;
    @BindView(R.id.customerTokoName)
    MaterialEditText customerToko;
    @BindView(R.id.customerPhone)
    MaterialEditText customerPhone;
    @BindView(R.id.customerEmail)
    MaterialEditText customerEmail;
    @BindView(R.id.customerAlamat)
    MaterialEditText customerAddress;
    @BindView(R.id.customerGrade)
    Spinner spinner;
    @BindView(R.id.customerWilayah)
    Spinner customerWilayah;
    @BindView(R.id.customerRekening)
    MaterialEditText customerRekening;
    @BindView(R.id.customerBank)
    MaterialEditText customerBank;
    @BindView(R.id.customerNPWP)
    MaterialEditText customerNPWP;
    @BindView(R.id.customerNpwpName)
    MaterialEditText customerNpwpName;
    @BindView(R.id.customerEkspedisi)
    MaterialEditText customerEkspedisi;
    @BindView(R.id.customerMobile)
    MaterialEditText customerMobile;
    @BindView(R.id.customerFax)
    MaterialEditText customerFax;

    private GoogleMap mMap;
    private Marker mMarker;
    private double latitude, longitude;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager locationManager;
    private LocationRequest mLocationRequest;

    File file;

    private static final int REQUEST_GALLERY_CODE = 200;

    private Uri uri;

    @BindView(R.id.btnUpload)
    ImageView imageView;

    Tracker gps;
    MarkerOptions markerOptions;
    String street;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    SweetAlertDialog swal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);

        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        gps = new Tracker(this);

        latitude = 0.00;
        longitude = 0.00;

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                openGalleryIntent.setType("image/*");
                startActivityForResult(openGalleryIntent, REQUEST_GALLERY_CODE);
            }
        });

        if (gps.canGetLocation()) {

            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

        } else {
            gps.showSettingsAlert();
            finish();
        }

        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                initSpinnerGrade();
                return false;
            }
        });

        customerWilayah.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                initSpinnerCity();
                return false;
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                swal = new SweetAlertDialog(CustomerActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                swal.setTitleText("Loading");
                swal.setContentText("Processing...");
                swal.setCancelable(false);
                swal.show();

                RequestBody token = RequestBody.create(MediaType.parse("text/plain"), Prefs.getString(AppConfig.KEY_TOKEN, ""));
                RequestBody code = RequestBody.create(MediaType.parse("text/plain"), customerCode.getText().toString());
                RequestBody name = RequestBody.create(MediaType.parse("text/plain"), customerName.getText().toString());
                RequestBody toko = RequestBody.create(MediaType.parse("text/plain"), customerToko.getText().toString());
                RequestBody alamat = RequestBody.create(MediaType.parse("text/plain"), customerAddress.getText().toString());
                RequestBody telp = RequestBody.create(MediaType.parse("text/plain"), customerPhone.getText().toString());
                RequestBody email = RequestBody.create(MediaType.parse("text/plain"), customerEmail.getText().toString());
                RequestBody grade = RequestBody.create(MediaType.parse("text/plain"), spinner.getSelectedItem().toString());
                RequestBody rekening = RequestBody.create(MediaType.parse("text/plain"), customerRekening.getText().toString());
                RequestBody bank = RequestBody.create(MediaType.parse("text/plain"), customerBank.getText().toString());
                RequestBody npwp = RequestBody.create(MediaType.parse("text/plain"), customerNPWP.getText().toString());
                RequestBody npwpName = RequestBody.create(MediaType.parse("text/plain"), customerNpwpName.getText().toString());
                RequestBody ekspedisi = RequestBody.create(MediaType.parse("text/plain"), customerEkspedisi.getText().toString());
                RequestBody mobile = RequestBody.create(MediaType.parse("text/plain"), customerMobile.getText().toString());
                RequestBody fax = RequestBody.create(MediaType.parse("text/plain"), customerFax.getText().toString());
                RequestBody wil = RequestBody.create(MediaType.parse("text/plain"), "Surabaya");
                RequestBody lat = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(latitude));
                RequestBody longi = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(longitude));
                RequestBody image = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part customerImg = MultipartBody.Part.createFormData("customerImg", file.getName(), image);

                postCustomer(token, code, name, toko, alamat, telp, email, grade, rekening, bank, npwp, npwpName, ekspedisi, mobile, fax, wil, lat, longi, customerImg);
            }
        });
    }

    private void postCustomer(RequestBody token, RequestBody customerCode, RequestBody name, RequestBody toko, RequestBody alamat, RequestBody telp, RequestBody email, RequestBody grade,
                              RequestBody rekening, RequestBody bank, RequestBody npwp, RequestBody namaNpwp, RequestBody ekspedisi, RequestBody mobile, RequestBody fax, RequestBody wilayah, RequestBody lat, RequestBody longi,
                              MultipartBody.Part customerImg) {
        ApiClient.get(this).createCustomer(token, customerCode, name, toko, alamat, telp, email, grade, rekening,bank,npwp,namaNpwp,ekspedisi,mobile,fax,wilayah,lat,longi,customerImg).enqueue(new Callback<ApiCustomer>() {
            @Override
            public void onResponse(Call<ApiCustomer> call, Response<ApiCustomer> response) {
                ApiCustomer resp = response.body();
                if (resp.getStatus() == "false") {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(CustomerActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                } else {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(CustomerActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    startActivity(new Intent(CustomerActivity.this, MainActivity.class));
                                    finish();
                                }
                            })
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiCustomer> call, Throwable t) {
                try {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(CustomerActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(t.getMessage())
                            .show();
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initSpinnerGrade() {
        ApiClient.get(this).getGrade(Prefs.getString(AppConfig.KEY_TOKEN, "")).enqueue(new Callback<ApiGrade>() {
            @Override
            public void onResponse(Call<ApiGrade> call, Response<ApiGrade> response) {
                ApiGrade resp = response.body();

                if (resp.getStatus() == "true") {
                    List<Grade> grades = resp.getGrades();
                    List<String> strings = new ArrayList<String>();

                    for (int i = 0; i < grades.size(); i++) {
                        strings.add(grades.get(i).getGrade_nama());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(CustomerActivity.this,
                            android.R.layout.simple_spinner_item, strings);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(adapter);
                } else {
                    new SweetAlertDialog(CustomerActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiGrade> call, Throwable t) {
                try {
                    swal.dismissWithAnimation();
                    new SweetAlertDialog(CustomerActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Notification")
                            .setContentText(t.getMessage())
                            .show();
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initSpinnerCity() {
        ApiClient.get(this).getCity(Prefs.getString(AppConfig.KEY_TOKEN, "")).enqueue(new Callback<ApiCity>() {
            @Override
            public void onResponse(Call<ApiCity> call, Response<ApiCity> response) {
                ApiCity resp = response.body();

                if (resp.getStatus() == "true") {
                    List<City> cities = resp.getCities();
                    List<String> strings = new ArrayList<String>();

                    for (int i = 0; i < cities.size(); i++) {
                        strings.add(cities.get(i).getCity_name());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(CustomerActivity.this, android.R.layout.simple_spinner_item, strings);
                    adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                    customerWilayah.setAdapter(adapter);

                } else {
                    new SweetAlertDialog(CustomerActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Notification")
                            .setContentText(resp.getMessage())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<ApiCity> call, Throwable t) {
                try {
                    throw new InterruptedException("Gagal Connect Server");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_GALLERY_CODE && resultCode == Activity.RESULT_OK){
            uri = data.getData();
            String filePath = getRealPathFromURIPath(uri, this);
            file = new File(filePath);

            imageView.setImageURI(data.getData());
        }
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mMap.setMyLocationEnabled(true);

        buildGoogleApiClient();
        //camera
        LatLng latLng = new LatLng(latitude, longitude);
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, 15);
        mMap.animateCamera(yourLocation);

        //marker
        markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Lokasi Saya");
        markerOptions.snippet("Lokasi Anda");
        markerOptions.draggable(true);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        mMarker = mMap.addMarker(markerOptions);

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                LatLng latLng = new LatLng(marker.getPosition().latitude, marker.getPosition().longitude);
                CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                mMap.animateCamera(yourLocation);
            }
        });

        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                LatLng latLng = new LatLng(latitude, longitude);
                mMarker.setPosition(latLng);

                return true;
            }

        });
    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {

        Integer clickCount = (Integer) marker.getTag();

        if (clickCount != null) {
            clickCount = clickCount + 1;
            marker.setTag(clickCount);
            Toast.makeText(this,
                    marker.getTitle() +
                            " has been clicked " + clickCount + " times.",
                    Toast.LENGTH_SHORT).show();
        }

        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
